$(document).ready(function() {
    $('.main-product__item__slider').slick({
        arrows: false,
        useTransform: true,
        autoplay: true,
        speed: 3000,
        cssEase: 'ease-in-out',
        fade: true,
        autoplaySpeed: 4500,
    })

    $('.product-page__content__img__slider').slick({
        asNavFor: '.product-page__preview-slider',
        arrows: false,
        fade: true
    })

    $('.product-page__preview-slider').slick({
        asNavFor: '.product-page__content__img__slider',
        slidesToShow: 5,
        focusOnSelect: true,
        swipeToSlide: true,
        prevArrow: '<span class="prev-arrow"><svg width="51" height="25" viewBox="0 0 51 25" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M51 12.5H1M1 12.5L12.5 1M1 12.5L12.5 24" stroke="currentColor"/></svg></span>',
        nextArrow: '<span class="next-arrow"><svg width="51" height="25" viewBox="0 0 51 25" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 12.5H50M50 12.5L38.5 1M50 12.5L38.5 24" stroke="currentColor"/></svg></span>',
        responsive: [{
                breakpoint: 1366,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 3,
                }
            }
        ]
    })

    $('.color-list a').on('click', function() {
        $(this).toggleClass('active').parent().siblings().find('a').removeClass('active');
    })

    // minus/plus
    $('.minus').on('click', function(e) {
        e.preventDefault()
        let $input = $(this).parent().find('.counter__input');
        let count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
    });
    $('.plus').on('click', function(e) {
        e.preventDefault()
        var $input = $(this).parent().find('.counter__input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
    });

    // delete cart item. Demo only
    $('.delete').on('click', function(e) {
        e.preventDefault();
        $(this).closest('tr').hide()
    })

    // video
    $('.play-video').on('click', function(e) {
        e.preventDefault();
    })
    $('.video').click(function() {
        var video = $('#video');
        var mediaVideo = $("#my-video").get(0);
        var playv = $(".video");
        if (mediaVideo.paused) {
            mediaVideo.play();
            mediaVideo.setAttribute("controls", "controls");
            playv.addClass("pause");
        } else {
            mediaVideo.pause();
            mediaVideo.removeAttribute("controls");
            playv.removeClass("pause");
        }
    });

    // menu
    $('.menu-btn').on('click', function() {
        $('body').addClass('open-menu')
    })
    $('.close-menu').on('click', function(e) {
        e.preventDefault()
        $('body').removeClass('open-menu')
    })

})